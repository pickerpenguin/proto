all: pico

pico login:
	mkdir ./src/$@
	protoc --go_out=./src/$@ --go_opt=module=gitlab.com/pickerpenguin/proto/$@ --go-grpc_out=./src/$@ --go-grpc_opt=module=gitlab.com/pickerpenguin/proto/$@ ./v1/$@.proto

